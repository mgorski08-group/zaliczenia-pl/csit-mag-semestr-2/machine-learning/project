import matplotlib.pyplot as plt
import numpy as np
from mlxtend.plotting import plot_decision_regions
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

import utils

x_data, y_data = utils.load_data()

target_names = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
x_data = x_data.reshape(x_data.shape[0], x_data.shape[1] * x_data.shape[2])
x_data = np.asarray(x_data / 255.0, dtype=np.float64)

# pca = PCA(n_components=2)
# x_pca = pca.fit(x_data).transform(x_data)

# plt.figure(figsize=(8, 6))
# for digit in range(10):
#     plt.scatter(x_pca[y_data == digit, 0], x_pca[y_data == digit, 1], label=str(digit))
# plt.legend(loc="best")
# plt.show()

x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.2, random_state=42, shuffle=True)

# scaler = StandardScaler()
#
# x_train = scaler.fit_transform(x_train)
# x_test = scaler.transform(x_test)


# Logistic regression
logreg = LogisticRegression(max_iter=1000)
logreg.fit(x_train, y_train)
y_pred_log = logreg.predict(x_test)
utils.show_scores(f1_score(y_test, y_pred_log, average=None))
utils.confusion_matrix_display(logreg, x_test, y_test, target_names, "Logistic regression")

# SVM
svm = SVC(kernel='rbf', max_iter=1000)
svm.fit(x_train, y_train)
y_pred_svm = svm.predict(x_test)
utils.show_scores(f1_score(y_test, y_pred_svm, average=None))
utils.confusion_matrix_display(svm, x_test, y_test, target_names, "SVM")

print(f"Accuracy logreg: {logreg.score(x_test, y_test)}")
print(f"Misclassified logreg: {sum(y_test != y_pred_log)}")
print("---"*10)

print(f"Accuracy SVM: {svm.score(x_test, y_test)}")
print(f"Misclassified SVM: {sum(y_test != y_pred_svm)}")
print("---"*10)
