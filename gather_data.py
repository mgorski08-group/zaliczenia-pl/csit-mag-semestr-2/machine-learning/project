import os
from tkinter import *
from PIL import Image
import io

# Create a canvas for drawing
canvas_width = 512
canvas_height = 512

output_dir = "raw_images"


def onKeyPress(event):
    if event.char not in "0123456789":
        return
    digit = event.char
    image = canvas.postscript(colormode='gray')
    img = Image.open(io.BytesIO(image.encode('utf-8')))

    os.makedirs(f"{output_dir}/{digit}", exist_ok=True)
    number = 0
    while os.path.exists(f"{output_dir}/{digit}/{number:03d}.png"):
        number += 1

    img.save(f"{output_dir}/{digit}/{number:03d}.png", format="PNG")
    canvas.delete("all")


def paint(event):
    x1, y1 = (event.x - 5), (event.y - 5)
    x2, y2 = (event.x + 5), (event.y + 5)
    canvas.create_oval(x1, y1, x2, y2, fill="black", width=20)


def clear():
    canvas.delete("all")


# Create the UI
root = Tk()

root.bind("<Key>", onKeyPress)

canvas = Canvas(root, width=canvas_width, height=canvas_height, bg='white')
canvas.pack(expand=YES, fill=BOTH)
canvas.bind("<B1-Motion>", paint)

clear_button = Button(root, text="Clear", command=clear)
clear_button.pack()

result_label = Label(root, text='')
result_label.pack()

root.mainloop()
