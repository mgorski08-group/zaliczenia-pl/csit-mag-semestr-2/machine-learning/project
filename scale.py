import os
from PIL import Image


# Function to scale and save an image
def scale_and_save_image(image_path, output_path, size):
    # Open the image
    image = Image.open(image_path)

    # Scale the image
    scaled_image = image.resize((size, size))
    # Save the scaled image
    scaled_image.save(output_path)


# Input and output directory paths
input_dir = "raw_images"
output_dirs = ["images/256", "images/128", "images/64", "images/32", "images/16", "images/8"]


# Recursive function to process images in subfolders
def process_images(input_dir, output_dir, size):
    # Loop over the files and subfolders in the input directory
    for item in os.listdir(input_dir):
        item_path = os.path.join(input_dir, item)

        # Check if the item is a file
        if os.path.isfile(item_path):
            # Scale and save the image
            output_path = os.path.join(output_dir, item)
            scale_and_save_image(item_path, output_path, size)
        # Check if the item is a subfolder
        elif os.path.isdir(item_path):
            # Create the corresponding output subfolder
            new_output_dir = os.path.join(output_dir, item)
            os.makedirs(new_output_dir, exist_ok=True)

            # Recursively process images in the subfolder
            process_images(item_path, new_output_dir, size)


# Loop over the output directories and corresponding sizes
for output_dir, size in zip(output_dirs, [256, 128, 64, 32, 16, 8]):
    # Process the images in each output directory
    process_images(input_dir, output_dir, size)
