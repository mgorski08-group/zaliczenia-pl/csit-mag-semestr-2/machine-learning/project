import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from sklearn.metrics import ConfusionMatrixDisplay


def show_scores(f1_scores):
    for digit, f1_score in enumerate(f1_scores):
        print(f"Digit: {digit} | F1-score = {f1_score:.4f}")
    print("---" * 10)


def confusion_matrix_display(clf, x_test, y_test, target_names, title):
    ConfusionMatrixDisplay.from_estimator(clf, x_test, y_test, display_labels=target_names, cmap=plt.cm.Blues,
                                          normalize="true")
    plt.title(title)
    plt.show()


def show_pca_kmeans(x_pca, cluster_labels, y_data):
    plt.figure(figsize=(10, 6))
    for digit in range(10):
        plt.scatter(x_pca[y_data == digit, 0], x_pca[y_data == digit, 1], alpha=0.5, label=str(digit))
    plt.xlabel("First principal component")
    plt.ylabel("Second principal component")
    plt.title("PCA Scatter Plot of Digits")
    plt.legend()
    plt.show()

    plt.figure(figsize=(10, 6))
    for digit in range(10):
        plt.scatter(x_pca[cluster_labels == digit, 0], x_pca[cluster_labels == digit, 1], alpha=0.5, label=str(digit))
    plt.xlabel("First Principal Component")
    plt.ylabel("Second Principal Component")
    plt.title("PCA Scatter Plot with K-means Clustering")
    plt.legend()
    plt.show()


def retrieve_images(folder_path):
    images = []
    labels = []
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            file_path = os.path.join(root, file)
            if file_path.endswith('.jpg') or file_path.endswith('.png'):
                image = Image.open(file_path).convert("L")
                image_array = 255 - np.array(image)
                images.append(image_array)

                # Add labels
                label = file_path[-9]
                labels.append(int(label))

    return images, labels


def load_data(images_folder="Images"):
    folder_list = next(os.walk(images_folder))[1]
    folder_list = sorted(folder_list, key=lambda x: int(x))

    print("Available folders:")
    for i, folder in enumerate(folder_list):
        print(f"{i + 1}. {folder}")

    choice = int(input("Enter the folder number to retrieve images from: ")) - 1
    chosen_folder = folder_list[choice]

    chosen_folder_path = os.path.join(images_folder, chosen_folder)

    image_array, labels = retrieve_images(chosen_folder_path)

    # To numpy array
    image_array = np.array(image_array)
    labels = np.array(labels)

    print(f"Total images retrieved: {len(image_array)}")
    print(f"Total labels retrieved: {len(labels)}")

    return image_array, labels
